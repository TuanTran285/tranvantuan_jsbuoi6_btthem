const btnTinh = document.getElementById("tinh-tien-thue");

function tinhTienThue() {
  const hoTen = document.getElementById("ho-ten").value;
  const thuNhapNam = document.getElementById("thu-nhap-nam").value * 1;
  const nguoiPhuThuoc = document.getElementById("nguoi-phu-thuoc").value * 1;
  const message = document.getElementById("message");
  // định nghĩa các biển trừ tiền
  const TIEN_TRU = 4e6;
  const TIEN_TRU_NGUOI_PHU_THUOC = 16e5;
  // định nghĩa các cột mốc tiền
  const SAU_MUOI_TRIEU = 60e6;
  const MOT_TRAM_HAI_MUOI_TRIEU = 120e6;
  const HAI_TRAM_MUOI_TRIEU = 210e6;
  const BA_TRAM_TAM_TU_TRIEU = 384e6;
  const SAU_TRAM_HAI_TU_TRIEU = 624e6;
  const CHIN_TRAM_SAU_MUOI_TRIEU = 960e6;

  if (thuNhapNam <= TIEN_TRU) {
    alert("Số tiền thu nhập không hợp lệ");
  } else {
    let tienThue = 0;
    if (thuNhapNam <= SAU_MUOI_TRIEU) {
      tienThue =
        (thuNhapNam - TIEN_TRU - nguoiPhuThuoc * TIEN_TRU_NGUOI_PHU_THUOC) *
        0.05;
    } else if (thuNhapNam <= MOT_TRAM_HAI_MUOI_TRIEU) {
      tienThue =
        (thuNhapNam - TIEN_TRU - nguoiPhuThuoc * TIEN_TRU_NGUOI_PHU_THUOC) *
        0.1;
    } else if (thuNhapNam <= HAI_TRAM_MUOI_TRIEU) {
      tienThue =
        (thuNhapNam - TIEN_TRU - nguoiPhuThuoc * TIEN_TRU_NGUOI_PHU_THUOC) *
        0.15;
    } else if (thuNhapNam <= BA_TRAM_TAM_TU_TRIEU) {
      tienThue =
        (thuNhapNam - TIEN_TRU - nguoiPhuThuoc * TIEN_TRU_NGUOI_PHU_THUOC) *
        0.2;
    } else if (thuNhapNam <= SAU_TRAM_HAI_TU_TRIEU) {
      tienThue =
        (thuNhapNam - TIEN_TRU - nguoiPhuThuoc * TIEN_TRU_NGUOI_PHU_THUOC) *
        0.25;
    } else if (thuNhapNam <= CHIN_TRAM_SAU_MUOI_TRIEU) {
      tienThue =
        (thuNhapNam - TIEN_TRU - nguoiPhuThuoc * TIEN_TRU_NGUOI_PHU_THUOC) *
        0.3;
    } else {
      tienThue =
        (thuNhapNam - TIEN_TRU - nguoiPhuThuoc * TIEN_TRU_NGUOI_PHU_THUOC) *
        0.35;
    }
    message.innerHTML = `=> Họ tên: ${hoTen}; Tiền thuế thu nhập cá nhân: ${tienThue.toLocaleString()}`;
  }
}

btnTinh.addEventListener("click", function () {
  tinhTienThue();
});

const loaiKhachHang = document.getElementById("loai-khach-hang");
loaiKhachHang.addEventListener("change", function () {
    const soKetNoi = document.querySelector(".form-sub");
    if(loaiKhachHang.value == "doanh-nghiep") {
        soKetNoi.style.display = "block";
    }else {
        soKetNoi.style.display = "none";
    }
  });

const btnTienCap = document.getElementById("tinh-tien-cap");

function tinhTienCap() {
    const loaiKhachHang = document.getElementById("loai-khach-hang").value;
    
    const maKhachHang = document.getElementById("ma-khach-hang").value;
    const kenhCaoCap = document.getElementById("so-kenh-cao-cap").value * 1;
    const soKetNoi = document.querySelector(".form-sub").value;
    const result = document.querySelector("#result")

  let thanhTien = 0;
  if (loaiKhachHang == "X") {
    alert("Vui lòng chọn loại khách hàng");
    return;
  } else if (loaiKhachHang == "nha-dan") {
    thanhTien = 4.5 + 20.5 + kenhCaoCap * 7.5;
  } else if (loaiKhachHang == "doanh-nghiep") {
    if(soKetNoi <= 10) {
        thanhTien = 15 + 75 + (kenhCaoCap * 50)
    }else {
        thanhTien = 15 + 75 + (kenhCaoCap * 50) + (soKetNoi - 10) * 5
    }
  }
  result.innerHTML = `=> Mã khách hàng:${maKhachHang};Tiền cáp: ${thanhTien.toLocaleString()}$`
}

btnTienCap.addEventListener("click", function () {
  tinhTienCap();
});
